const mysql = require('mysql')

const connection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'blog'
})

let postModel = {}

postModel.getPosts = (callback) => {
    if(connection){
        connection.query("SELECT * FROM posts ORDER BY created_at DESC",(err,rows)=>{
            if(err){
                throw err
            }else{
                callback(null,rows)
            }

        })
    }
}

postModel.getPostResponses = (parms,callback) => {
    if(connection){
        connection.query(`SELECT * FROM responses WHERE post_id = ${connection.escape(parms.post_id)}`,(err,rows)=>{
            if(err){
                throw err
            }else{
                callback(null,rows)
            }

        })
    }
}
postModel.storePost = (post_content,callback) => {
    if(connection){
        connection.query(`INSERT INTO blog.posts
        (responses_count, post_content, created_at, updated_at)
        VALUES(0, ${connection.escape(post_content)}, current_timestamp, current_timestamp);
        `,(err,rows)=>{
            if(err){
                throw err
            }else{
                connection.query(`select * from posts where post_content = ${connection.escape(post_content)} order by post_id DESC limit 1`,(err,rows)=>{
                        if(err){
                            throw err
                        }else{
                            callback(null,rows[0])
                        }
                    })
            }
        })
    }
}
postModel.storeResponse = ({response_content,post_id},callback) => {
    if(connection){
        connection.query(`INSERT INTO blog.responses
            (post_id, response_content, created_at, updated_at)
            VALUES(${connection.escape(post_id)}, ${connection.escape(response_content)}, current_timestamp, current_timestamp);
        `,(err,rows)=>{
            if(err){
                throw err
            }else{
                connection.query(`update posts set responses_count = responses_count + 1 where post_id = ${connection.escape(post_id)} `,(err,rows)=>{
                        if(err){
                            throw err
                        }else{
                              connection.query(`select * from responses where post_id = ${connection.escape(post_id)} order by response_id desc`,(err,rows)=>{
                                  if(err){
                                      throw err
                                  }else{
                                      callback(null,rows[0])
                                  }
                              })
                        }
                    })
            }
        })
    }
}

module.exports = postModel
