const express   = require('express')
const morgan    = require('morgan')
const cors      = require('cors')

const app = express()


//settings
app.set('port',process.env.PORT || 80)
app.set('json spaces',2)

//middlewares
app.use(morgan('dev'))
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(cors())
//routes
app.use('/api/posts',require('./routes/posts'))

app.listen(app.get('port'), ()=>{
    console.log(`Server on port ${app.get('port')}`)
})
