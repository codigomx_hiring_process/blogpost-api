const {Router} = require('express')
const Post = require('../models/posts')
const router = Router()

router.get('/',(req,res)=>{
    Post.getPosts((err,data)=>{
        res.status(200).json(data);
    })
})

router.post('/',(req,res)=>{
  try{
    const {post_content} = req.body
    Post.storePost(post_content,(err,data)=>{
        res.status(201).json(data)
    })
  }catch(e){
    return res.status(400).json({message:"Bad request: post_content is required"})
  }
})


router.get('/responses',(req,res)=>{
    const {post_id} = req.query
    if(!post_id){
        return res.status(400).json({message:"Bad request: post_id is required"})
    }
    Post.getPostResponses({post_id},(err,data)=>{
        res.status(200).json(data)
    })
})

router.post('/responses',(req,res)=>{
    console.log({req:req.body,res:req.query})
    try{
      const {response_content,post_id} = req.body
      Post.storeResponse({response_content,post_id},(err,data)=>{
          res.status(201).json(data)
      })
    }catch(e){
      return res.status(400).json({message:"Bad request: response_content & post_id is required"})
    }
})

module.exports = router
